<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php 
           include('include/body.php');
           ?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Kategori </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                       <a href="#kategoritambah" class="btn btn-success" data-toggle="modal">Tambah Kategori</a><br><br>
                       <div class="modal" id="kategoritambah">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Tambah Kategori</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proseskategori?aksi=tambah" enctype="multipart/form-data" class="form-horizontal form-material"">
                        <div class="box-body">
                            <div class="form-group">
                                    <label for="nama_kategori">Nama Kategori :</label>
                                      <input type="text" id="nama_kategori" class="form-control" placeholder="Masukkan Nama Kategori" name="nama_kategori" required>
                                </div>
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kategori</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                         <tr>
                                            <th>No</th>
                                            <th>Nama Kategori</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                   <?php
                                    $no = 1;
                                    foreach($db->tampil_data_kategori() as $x){
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['nama_kategori']; ?></td>
                                            <td>
                                              <a href="#" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal<?php echo $x['id_kategori'];?>">Edit</a>     
                                           </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                   </table>
                            </div>
                            <?php
include "../koneksi.php";
$no=0;
$data = "SELECT * from kategori";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
  ?>
  <?php
$id = $select_result['id_kategori']; 
$query_edit = mysqli_query($conn,"SELECT * FROM kategori WHERE id_kategori='$id'");
$r = mysqli_fetch_array($query_edit);
?>
            <div class="modal" id="myModal<?php echo $select_result['id_kategori'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Kategori</h4>
                  </div>

                  <div class="modal-body">
                    <form role="form"  method="POST" action="proseskategori?aksi=update" enctype="multipart/form-data" class="form-horizontal form-material"">
                            <div class="form-group">
                                    <label for="username">Nama Kategori :</label>
                                      <input type="hidden" name="id_kategori" value="<?php echo $r['id_kategori']?>">
                                      <input type="text" id="nama_kategori" class="form-control" placeholder="Masukkan Nama Kategori" name="nama_kategori" value="<?php echo $r['nama_kategori']?>">
                                </div>
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
<?php } ?>
                       </div>
                   </div>
               </div>
        <!-- End Wrapper-->
        </div>

        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
