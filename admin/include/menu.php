<ul class="nav nav-pills nav-stacked custom-nav">
                <li class=""><a href="index"><i class="icon-home"></i> <span>Dashboard</span></a>
                </li>

                <li class="menu-list"><a href="#"><i class="icon-layers"></i> <span>Master Data</span></a>
                    <ul class="sub-menu-list" style="background: #202427;">
                        <li><a href="pengguna">Pengguna</a></li>
                        <li><a href="data_meja">Data Meja</a></li>
                        <li><a href="data_makanan">Data Makanan dan Minuman</a></li>
                        <li><a href="data_kategori">Data Kategori</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href="#"><i class="fa fa-spoon"></i> <span>Order</span></a>
                    <ul class="sub-menu-list" style="background: #202427;">
                        <li><a href="data_order">Data Order Belum Terbayar</a></li>
                        <li><a href="data_order_terbayar">Data Order yang Sudah Terbayar</a></li>
                        <li><a href="pesan_meja">Pemesanan</a></li>
                    </ul>
                </li>
                <li class=""><a href="transaksi"><i class="fa fa-money"></i> <span>Transaksi</span></a>
                </li>

                 <li class="menu-list"><a href="#"><i class="fa fa-print"></i> <span>Laporan</span></a>
                    <ul class="sub-menu-list" style="background: #202427;">
                        <li><a href="laporan">Laporan Transaksi</a></li>
                        <li><a href="laporan_data_order">Laporan Data Order</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href="#"><i class="icon-settings"></i> <span>Pengaturan</span></a>
                    <ul class="sub-menu-list" style="background: #202427;">
                        <li><a href="backup_data">Backup Database</a></li>
                    </ul>
                </li>

            </ul>