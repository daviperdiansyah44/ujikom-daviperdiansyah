<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php 
           include('include/body.php');
           ?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Makanan dan Minuman </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
        </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                       <div class="box-header">
                  <a href="#masakantambah" class="btn btn-success" data-toggle="modal">Tambah Menu</a><br><br>
                  <a href="lap_makanan" class="btn btn-danger" target="blank">Export ke PDF</a><br><br>
                </div><br>
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                     <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Masakan</th>
                        <th>Jenis</th>
                        <th>Kategori</th>
                        <th>Gambar</th>
                        <th>Harga</th>
                        <th>Status Masakan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
include "../koneksi.php";
$no=0;
$data = "SELECT * from masakan inner join kategori on masakan.id_kategori = kategori.id_kategori";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
$no++;
$nama_masakan        = $select_result['nama_masakan'];
$jenis               = $select_result['jenis'];
$kategori            = $select_result['nama_kategori'];
$gambar              = $select_result['image'];
$harga               = $select_result['harga'];
$status_masakan      = $select_result['status_masakan'];

echo"<tr>
<td>$no</td> 
<td>$nama_masakan</td>
<td>$jenis</td>
<td>$kategori</td>
<td><img src='gambar/$gambar' height='100'></td>
<td>$harga</td>
";
//ganti imagesup dengan nama folder dimana anda menempatkan image hasil upload
?>
<td>                                        <?php
                                            if($select_result['status_masakan'] == 'Y')
                                            {
                                              ?>
                                            <a href="approve_makanan?table=masakan&id_masakan=<?php echo $select_result['id_masakan']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Tersedia
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                            <a href="approve_makanan?table=masakan&id_masakan=<?php echo $select_result['id_masakan']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Habis
                                            </a>
                                            <?php
                                            }
                                            ?></td>
<td>
  <a href="#" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal<?php echo $select_result['id_masakan'];?>">Edit</a>
</td>
</tr>
<div class="modal" id="myModal<?php echo $select_result['id_masakan'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Menu</h4>
                  </div>
<?php
$id = $select_result['id_masakan']; 
$query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id'");
$r = mysqli_fetch_array($query_edit);
?>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="update_makanan?id_masakan=<?php echo $r['id_masakan'];?>" enctype="multipart/form-data" class="form-horizontal form-material"">
                            <div class="form-group">
                              <label for="nama_masakan">Nama Masakan</label>
                              <input type="hidden" name="id_masakan" value="<?php echo $r['id_masakan'];?>">
                              <input type="text" class="form-control" name="nama_masakan" id="nama_masakan" value="<?php echo $r['nama_masakan'];?>" placeholder="Masukan Nama" required>
                            </div>
                            <div class="form-group">
                            <label>Jenis :</label><br>
                            <select name="jenis" class="required">
                              <option>Pilih Jenis</option>
                              <option value="Makanan" <?=$r['jenis']=='Makanan'?'selected':null;?>>Makanan</option>
                              <option value="Minuman" <?=$r['jenis']=='Minuman'?'selected':null;?>>Minuman</option>
                              </select>
                            </div>
                               <div class="form-group">
                                 <label for="id_kategori">Nama Kategori :</label><br>
                                    <select name="id_kategori" class="form-control col-md-12">
                                    <?php     
                                    include"../koneksi.php";
                                      $select=mysqli_query($conn, "SELECT * FROM kategori");
                                      while($show=mysqli_fetch_array($select)){
                                    ?>
                                      <option value="<?=$show['id_kategori'];?>" <?=$r['id_kategori']==$show['id_kategori']?'selected':null?>><?=$show['nama_kategori'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                            <div class="form-group">
                              <label for="harga">Harga</label>
                              <input type="integer" class="form-control" name="harga" id="harga" value="<?php echo $r['harga'];?>" placeholder="Masukan Harga">
                            </div>
                            <div class="form-group">
                              <label for="image">Masukan Gambar</label>
                              <input type="file" name="image" value="gambar/<?php echo $r['image'];?>">
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
<?php }  ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->




      <div class="modal" id="masakantambah">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Tambah Menu</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses_gambar" enctype="multipart/form-data" class="form-horizontal form-material"">
                        <div class="box-body">
                            <div class="form-group">
                              <label for="nama_masakan">Nama Masakan</label>
                              <input type="text" class="form-control" name="nama_masakan" id="nama_masakan" placeholder="Masukan Nama Masakan" required>
                            </div>
                             <div class="form-group">
                            <label>Jenis :</label><br>
                              <select name="jenis" class="required">
                              <option>Pilih Jenis</option>
                              <option value="Makanan">Makanan</option>
                              <option value="Minuman">Minuman</option>
                              </select>
                            </div>
                            <div class="form-group">
                                    <label for="id_kategori">Nama Kategori :</label><br>
                                    <select name="id_kategori">
                                       <option>Pilih Nama Kategori</option>
                                          <?php     
                                            include"../koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM kategori");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                                      <option value="<?=$show['id_kategori'];?>"><?=$show['nama_kategori'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                            <div class="form-group">
                              <label for="harga">Harga</label>
                              <input type="text" class="form-control" name="harga" id="harga" placeholder="Masukan Harga" required>
                            </div>
                            <div class="form-group">
                              <label for="image">Masukan Gambar</label>
                              <input type="file" accept="image/*" name="image" required>
                            </div>
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
