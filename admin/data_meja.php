<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php 
           include('include/body.php');
           ?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Meja </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                       <a href="#kategoritambah" class="btn btn-success" data-toggle="modal">Tambah Meja</a><br><br>
                       <div class="modal" id="kategoritambah">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Tambah Meja</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="prosesmeja?aksi=tambah" enctype="multipart/form-data" class="form-horizontal form-material"">
                        <div class="box-body">
                            <div class="form-group">
                                    <label for="no_meja">No. Meja :</label>
                                      <input type="text" id="no_meja" class="form-control" placeholder="Masukkan Nomor Meja" name="no_meja" required>
                                </div>
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No. Meja</th>
                                            <th>Status Meja</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                         <tr>
                                            <th>No</th>
                                            <th>No. Meja</th>
                                            <th>Status Meja</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                   <?php
                                    $no = 1;
                                    foreach($db->tampil_data_meja() as $x){
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['no_meja']; ?></td>
                                            <td><?php echo $x['status_meja']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                   </table>
                            </div>
                            <?php
include "../koneksi.php";
$no=0;
$data = "SELECT * from meja";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
  ?>
  <?php
$id = $select_result['id_meja']; 
$query_edit = mysqli_query($conn,"SELECT * FROM meja WHERE id_meja='$id'");
$r = mysqli_fetch_array($query_edit);
?>
            <div class="modal" id="myModal<?php echo $select_result['id_meja'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Meja</h4>
                  </div>

                  <div class="modal-body">
                    <form role="form"  method="POST" action="prosesmeja?aksi=update" enctype="multipart/form-data" class="form-horizontal form-material"">
                            <div class="form-group">
                                    <label for="username">No Meja :</label>
                                      <input type="hidden" name="id_meja" value="<?php echo $r['id_meja']?>">
                                      <input type="text" id="no_meja" class="form-control" placeholder="Masukkan No. Meja" name="no_meja" value="<?php echo $r['no_meja']?>" required>
                                </div>
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
<?php } ?>
                       </div>
                   </div>
               </div>
        <!-- End Wrapper-->
        </div>

        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
