<?php
include '../koneksi.php';
require('../assets/fpdf.php');

$pdf = new FPDF("P","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(8.6); 
$pdf->SetFont('Arial','B',14);           
$pdf->MultiCell(10,0.5,'Struk D`Resto',0,'L');
$pdf->SetFont('Arial','B',10);
$pdf->SetX(6.5);
$pdf->MultiCell(10,0.5,'Jl. Paledang Kp. Karamat RT 05 RW 01 No. 26',0,'L');
$pdf->SetX(6.3);
$pdf->MultiCell(19.5,0.5,'website : www.dresto.com : dresto@gmail.com',0,'L');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$get_id = $_GET['id_order'];
$query2 = mysqli_query($conn, "SELECT * FROM transaksi INNER JOIN user ON transaksi.id_user = user.id_user where transaksi.id_order = '$get_id'");
$lihat4 = mysqli_fetch_array($query2);
$pdf->Cell(5,0.7,"Tanggal : ".date("D-d/m/Y"),0,0,'C');
$pdf->Cell(19,0.7,"Kasir : ".$lihat4['nama_user'],0,0,'C');

$pdf->ln(1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'No', 0, 0, 'C');
$pdf->Cell(5, 0.8, 'Nama Menu', 0, 0, 'C');
$pdf->Cell(4, 0.8, 'Harga', 0, 0, 'C');
$pdf->Cell(4, 0.8, 'Kuantitas', 0, 0, 'C');
$pdf->Cell(4, 0.8, 'Total Harga', 0, 1, 'C');


$pdf->SetFont('Arial','',10);
$no=1;
$id_get_order_id=$_GET['id_order'];
$query=mysqli_query($conn,"SELECT * from oder inner join detail_order on oder.id_order=detail_order.id_order inner join masakan on detail_order.id_masakan=masakan.id_masakan inner join transaksi on oder.id_order=transaksi.id_order where oder.id_order='$id_get_order_id'");
while($lihat=mysqli_fetch_array($query)){
	 $jumlah=$lihat['jumlah']*$lihat['harga'];
	$harga=$lihat['harga'];
	 $hasil="Rp. ".number_format($harga,2,',','.');
	  $hasil_jumlah="Rp. ".number_format($jumlah,2,',','.');
	$pdf->Cell(1, 0.8, $no , 0, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['nama_masakan'],0, 0, 'C');
	$pdf->Cell(4, 0.8, $hasil, 0, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['jumlah'],0, 0, 'C');
	$pdf->Cell(4, 0.8, $hasil_jumlah,0, 1, 'C');


	$no++;
}
$query1=mysqli_query($conn,"SELECT * from transaksi where id_order='$id_get_order_id'");
$total_bayar=mysqli_fetch_array($query1);
	$harga=$total_bayar['total_bayar'];
	 $hasil="Rp. ".number_format($harga,2,',','.');
	 $harga1=$total_bayar['jumlah_uang'];
	 $jumlah_uang="Rp. ".number_format($harga1,2,',','.');
	 $harga2=$total_bayar['kembalian'];
	 $kembalian="Rp. ".number_format($harga2,2,',','.');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->SetX(8.65);
$pdf->Cell(16.5,0.7,"Total 			:           ".$hasil,0,0,'C');

$pdf->ln(1);
$pdf->SetX(8);
$pdf->Cell(16.5,0.7,"Jumlah Uang 	:          ".$jumlah_uang,0,0,'C');

$pdf->ln(1);
$pdf->SetX(8);
$pdf->Cell(16.5,0.7,"Kembalian 		:         ".$kembalian,0,0,'C');









$pdf->Output("struk.pdf","I");

?>

