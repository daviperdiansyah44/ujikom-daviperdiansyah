<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php 
           include('include/body.php');
           ?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Order </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
          <?php
          if(isset($_GET['pesan'])){
            $ambil_pesan = $_GET['pesan'];
            if($ambil_pesan=="berhasil"){
              echo "<div class = 'alert alert-success'>Transaksi Berhasil dilakukan</div>";
            }else if($ambil_pesan=="gagal"){
              echo "<div class = 'alert alert-danger'>Transaksi Gagal dilakukan atau Uang yang anda Masukan kurang dari Jumlah Pembayaran! Silahkan Lakukan Transaksi Kembali!</div>";
            }
          }
          ?> 
          <?php
          include "../koneksi.php";
          $id_get_order=$_GET['id_order'];
          $query=mysqli_query($conn,"SELECT id_order from oder where id_order='$id_get_order'");
$r=mysqli_fetch_array($query);
$id_order=  $r['id_order'];

        ?>
          <tr>
            <td width="80">
          <a href="transaksi" class="btn btn-danger">Kembali</a></p>
           </td>
           <td>
            <a href="struk_cetak?id_order=<?php echo $id_order;?>" target="_blank" class="btn btn-primary">Cetak Struk</a></p>
          </td>
          </tr>
               <h3 align="center"><b> Struk D'Restoran</b></h3>
       <div class="box-body">
       <table border="0" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Masakan</th>
                  <th>Harga</th>
                  <th>Quantity</th>
                  <th>Sub Harga</th>
                </tr>
              </thead>
                <tbody>
        <?php
              $no=1;
$id_get_order_id=$_GET['id_order'];
$query=mysqli_query($conn,"SELECT * from oder inner join detail_order on oder.id_order=detail_order.id_order inner join masakan on detail_order.id_masakan=masakan.id_masakan inner join transaksi on oder.id_order=transaksi.id_order where oder.id_order='$id_get_order_id'");
while($x=mysqli_fetch_array($query)){
   $jumlah=$x['jumlah']*$x['harga'];
  $harga=$x['harga'];
  $hasil="Rp".number_format($harga,2,',','.');
  $hasil_kali="Rp".number_format($jumlah,2,',','.');
    $total_jumlah=$x['total_bayar'];
    $total_jumlah_bayar="Rp".number_format($total_jumlah,2,',','.');
    $jumlah_uang=$x['jumlah_uang'];
    $jumlah_uang_format="Rp".number_format($jumlah_uang,2,',','.');
    $kembalian=$x['kembalian'];
    $kembalian_format="Rp".number_format($kembalian,2,',','.');

      ?>
                  <tr>
                    <td><?php echo $no++;?></td>
                    <td><?php echo $x['nama_masakan'];?></td>
                   
                    <td><?php echo $hasil;?></td>
                     <td><?php echo $x['jumlah'];?></td>
                    <td><?php echo $hasil_kali;?></td>

                  </tr>
                 

              
               
              <tr>
                <?php

}
?>
              
  <td colspan="4" align="right"><h4><b>Total Pembayaran</b></h4></td>
  <td ><h4><?php echo $total_jumlah_bayar;?></h4></td>
 
  </tr>
   <tr>
  <td colspan="4" align="right"><h4><b>Jumlah Uang</b></h4></td>
  <td ><h4><?php echo $jumlah_uang_format;?></h4></td>
 
  </tr>
   <tr>
  <td colspan="4" align="right"><h4><b>Kembalian</b></h4></td>
  <td ><h4><?php echo $kembalian_format;?></h4></td>
 
  </tr>

  </tbody>
            </table>

            
 
                 
                </div>
              </div>
            
           
    </div>
    </section>
    </div>
    </div>


        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
