<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php 
           include('include/body.php');
           ?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Laporan Tanggal Transaksi</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div><br>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                            <div class="table-responsive">
                            <form action="laporan_data_order" method="post" name="postform">
            <p align="center"><font color="orange" size="3"><b>Pencarian Data Berdasarkan Periode Tanggal</b></font></p><br />
            <table border="0">
                <tr>
                    <td width="125"><b>Dari Tanggal</b></td>
                    <td colspan="2" width="190">: <input type="date" name="tanggal_awal" size="16" />
                    </td>
                    <td width="125"><b>Sampai Tanggal</b></td>
                    <td colspan="2" width="190">: <input type="date" name="tanggal_akhir" size="16" />
                    </td>
                    <td colspan="2" width="190"><input type="submit" value="Pencarian Data" name="pencarian"/></td>
                    <td colspan="2" width="70"><input type="reset" value="Reset" /></td>
                </tr>
            </table>
        </form>
                               <table id="example" class="display table">
                                    <thead>
                                    <?php
                                    $no = 1;
                                    //proses jika sudah klik tombol pencarian data
                                    if(isset($_POST['pencarian'])){
                                    //menangkap nilai form
                                    $tanggal_awal=$_POST['tanggal_awal'];
                                    $tanggal_akhir=$_POST['tanggal_akhir'];
                                    if(empty($tanggal_awal) || empty($tanggal_akhir)){
                                    //jika data tanggal kosong
                                    ?>
                                    <script language="JavaScript">
                                        alert('Tanggal Awal dan Tanggal Akhir Harap di Isi!');
                                        document.location='laporan_data_order';
                                    </script>
                                    <?php
                                    }else{
                                    ?><i><b>Informasi : </b> Hasil pencarian data berdasarkan periode Tanggal <b><?php echo $_POST['tanggal_awal']?></b> s/d <b><?php echo $_POST['tanggal_akhir']?></b></i>
                                    <?php
                                    $query=mysqli_query($conn,"SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user,oder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order where oder.tanggal between '$tanggal_awal' and '$tanggal_akhir'");
                                    }
                                ?>
                                        <tr>
                                            <th>No</th>
                                            <th>No. Meja</th>
                                            <th>Tanggal</th>
                                            <th>Nama User</th>
                                            <th>Status Order</th>
                                            <th>Keterangan Transaksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                         <tr>
                                            <th>No</th>
                                            <th>No. Meja</th>
                                            <th>Tanggal</th>
                                            <th>Nama User</th>
                                            <th>Status Order</th>
                                            <th>Keterangan Transaksi</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php
                                        //menampilkan pencarian data
                                        while($x=mysqli_fetch_array($query)){
                                        ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['no_meja']; ?></td>
                                            <td><?php echo $x['tanggal']; ?></td>
                                            <td><?php echo $x['nama_user']; ?></td>
                                            <td><?php
                                            if($x['status_order'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Sudah di Terima";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum di Terima";?>
                                            <?php 
                                            }
                                            ?></td>
                                            <<td><?php
                                            if($x['keterangan_transaksi'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Sudah Terbayar";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum Terbayar";?>
                                            <?php 
                                            }
                                            ?></td>
                                        </tr>

                                    <?php 
                                    } ?>
                                    </tbody>

        </table>
        <a href="lap_order?tanggal_awal=<?php echo $_POST['tanggal_awal']?>&tanggal_akhir=<?php echo $_POST['tanggal_akhir']?>" class="btn btn-danger" target="blank">Cetak</a>
        <?php
        }
        else{
            unset($_POST['pencarian']);
        }
        ?>
        
                            </div>
                       </div>
                   </div>

               </div>
        <!-- End Wrapper-->
        </div>

        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
