<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="2"){
  header("location:waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:pelanggan/index");
}
?>
<?php
include'database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php 
           include('include/body.php');
           ?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
           <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['username']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Pengguna </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
             <div class="col-md-4">
                        <div class="panel panel-color panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Backup Database</h3>
                            </div>
                            <div class="panel-body">
                            <?php
                              error_reporting(0);
                              $file=date("Ymd").'_backup_database_'.time().'.sql';
                              backup_tables("localhost","root","","kasir",$file);
                            ?>
                                <div class="form-group pull-right">
                                  <a style="cursor:pointer" onclick="location.href='download_backup_data?nama_file=<?php echo $file;?>'" title="Download" class="icon-layers" >&nbsp;Backup</a>
                                </div> 
                                <?php
            /*
            untuk memanggil nama fungsi :: jika anda ingin membackup semua tabel yang ada didalam database, biarkan tanda BINTANG (*) pada variabel $tables = '*'
            jika hanya tabel-table tertentu, masukan nama table dipisahkan dengan tanda KOMA (,) 
            */
            function backup_tables($host,$user,$pass,$name,$nama_file,$tables ='*') {
              $link = mysql_connect($host,$user,$pass);
              mysql_select_db($name,$link);
              
              if($tables == '*'){
                $tables = array();
                $result = mysql_query('SHOW TABLES');
                while($row = mysql_fetch_row($result)){
                  $tables[] = $row[0];
                }
              }
              else{//jika hanya table-table tertentu
                $tables = is_array($tables) ? $tables : explode(',',$tables);
              }
              
              foreach($tables as $table){
                $result = mysql_query('SELECT * FROM '.$table);
                $num_fields = mysql_num_fields($result);
                                
                $return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
                $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
                $return.= "\n\n".$row2[1].";\n\n";
                
                for ($i = 0; $i < $num_fields; $i++) {
                  while($row = mysql_fetch_row($result)){
                    //menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                    for($j=0; $j<$num_fields; $j++) {
                      //akan menelusuri setiap baris query didalam
                      $row[$j] = addslashes($row[$j]);
                      $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                      if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                      if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                  }
                }
                $return.="\n\n\n";
              }             
              //simpan file di folder
              $nama_file;
              
              $handle = fopen('backup/'.$nama_file,'w+');
              fwrite($handle,$return);
              fclose($handle);
            }
            ?>
                            </div>
                        </div>
                    </div>

               </div>
        <!-- End Wrapper-->
        </div>

        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
