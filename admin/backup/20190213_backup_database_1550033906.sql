DROP TABLE detail_order;

CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_detail_order` enum('selesai','di proses') NOT NULL,
  PRIMARY KEY (`id_detail_order`),
  KEY `id_order` (`id_order`),
  KEY `id_masakan` (`id_masakan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE kategori;

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(30) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO kategori VALUES("1","Mie");
INSERT INTO kategori VALUES("5","Minuman");
INSERT INTO kategori VALUES("6","Nasi");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","waiter");
INSERT INTO level VALUES("3","kasir");
INSERT INTO level VALUES("4","owner");
INSERT INTO level VALUES("5","pelanggan");



DROP TABLE masakan;

CREATE TABLE `masakan` (
  `id_masakan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_masakan` varchar(50) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `jenis` enum('Makanan','Minuman','','') NOT NULL,
  `harga` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status_masakan` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Habis, Y Tersedia',
  PRIMARY KEY (`id_masakan`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

INSERT INTO masakan VALUES("16","Mie Aceh","1","Makanan","15000","IMG-20161025-WA0007.jpg","N");
INSERT INTO masakan VALUES("17","MIe Goreng","1","Makanan","6000","IMG-20161025-WA0015.jpg","N");
INSERT INTO masakan VALUES("18","Nasi Goreng","6","Makanan","15000","IMG-20161028-WA0001.jpg","Y");
INSERT INTO masakan VALUES("19","Teh Manis","5","Minuman","5000","IMG-20161025-WA0017.jpg","N");
INSERT INTO masakan VALUES("20","l","5","Makanan","5000","IMG-20161025-WA0006.jpg","Y");



DROP TABLE oder;

CREATE TABLE `oder` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `no_meja` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `status_order` enum('sedang di pesan','sedang di masak','sedang di anter') NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO oder VALUES("1","3","2019-02-13","11","apa","sedang di pesan");



DROP TABLE recovery_keys;

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO recovery_keys VALUES("1","1","94540c8b02feb18a4a522ce664cae561","1");
INSERT INTO recovery_keys VALUES("2","2","e436d7893a671386484ff33d110ea959","1");
INSERT INTO recovery_keys VALUES("3","1","cbc55f26f476d9183eb33156bd4c922b","0");
INSERT INTO recovery_keys VALUES("4","1","ae39191fab140225e8a1a56709f942ad","0");
INSERT INTO recovery_keys VALUES("5","1","10e3a403ad9d84fef2c21c4b491661fd","0");
INSERT INTO recovery_keys VALUES("6","1","de1167383d9a2abd34b44dcc38853a2a","0");
INSERT INTO recovery_keys VALUES("7","1","98c3ad70246159a7ae8cb2068c43bff3","0");



DROP TABLE transaksi;

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(12) NOT NULL,
  PRIMARY KEY (`id_transaksi`),
  KEY `id_user` (`id_user`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE user;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y sudah Aktif',
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO user VALUES("2","waiter","caf1a3dfb505ffed0d024130f58c5cfa","waiter@gmail.com","waiter","N","2");
INSERT INTO user VALUES("3","kasir","de28f8f7998f23ab4194b51a6029416f","","kasir","N","3");
INSERT INTO user VALUES("4","owner","5be057accb25758101fa5eadbbd79503","","owner","N","4");
INSERT INTO user VALUES("11","pelanggan","7f78f06d2d1262a0a222ca9834b15d9d","dappp@gmail.com","dappp","N","5");
INSERT INTO user VALUES("20","maulana","aff4b352312d5569903d88e0e68d3fbb","maulana@gmail.com","maulana fatullah","N","2");
INSERT INTO user VALUES("21","Budi","00dfc53ee86af02e742515cdcf075ed3","budi@gmail.com","Budi","Y","5");
INSERT INTO user VALUES("22","daviperdiansyah","ada64730d04b7fc05a2968f22ba63d41","daviperdiansyah44@gmail.com","Davi Perdiansyah","Y","1");
INSERT INTO user VALUES("23","kevine","8ce4b16b22b58894aa86c421e8759df3","kevine12@gmail.com","kevin atami tanos","Y","2");
INSERT INTO user VALUES("25","p","83878c91171338902e0fe0fb97a8c47a","p@gmail.com","p","N","5");
INSERT INTO user VALUES("26","admin","21232f297a57a5a743894a0e4a801fc3","daviperdiansyah44@gmail.com","Dav","Y","1");



