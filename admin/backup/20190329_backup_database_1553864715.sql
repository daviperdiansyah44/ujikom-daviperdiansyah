DROP TABLE detail_order;

CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_detail_order` varchar(30) NOT NULL,
  PRIMARY KEY (`id_detail_order`),
  KEY `id_order` (`id_order`),
  KEY `id_masakan` (`id_masakan`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;

INSERT INTO detail_order VALUES("125","93","17","2","uiuiui","Y");
INSERT INTO detail_order VALUES("126","93","18","1","iuiuiuiuiuiuiuiu","Y");
INSERT INTO detail_order VALUES("127","94","19","2","jangan pake teh hehehe","Y");
INSERT INTO detail_order VALUES("128","93","19","1","hehehee","N");
INSERT INTO detail_order VALUES("129","94","16","1","uuu","N");
INSERT INTO detail_order VALUES("130","93","18","1","kk","N");
INSERT INTO detail_order VALUES("131","93","16","1","mimimimimimimi","N");
INSERT INTO detail_order VALUES("132","94","20","1","jangan pake es","N");
INSERT INTO detail_order VALUES("133","94","17","1","bubububububububbubububu","N");
INSERT INTO detail_order VALUES("134","95","18","1","iuiuiui","Y");
INSERT INTO detail_order VALUES("135","96","18","1","mimimimmimii","Y");
INSERT INTO detail_order VALUES("136","97","19","1","ooop","Y");
INSERT INTO detail_order VALUES("137","98","28","1","jangan pake cabe","Y");
INSERT INTO detail_order VALUES("138","95","19","1","gulanya sedikit saja","N");
INSERT INTO detail_order VALUES("139","95","17","1","punya davi","N");
INSERT INTO detail_order VALUES("140","99","19","1","haik","Y");
INSERT INTO detail_order VALUES("141","100","19","1","hehehee","Y");
INSERT INTO detail_order VALUES("142","101","18","1","hehehehe","Y");
INSERT INTO detail_order VALUES("143","102","16","2","ninininini","Y");
INSERT INTO detail_order VALUES("144","102","18","1","nunununu","Y");
INSERT INTO detail_order VALUES("145","95","18","1","ashiappp","Y");
INSERT INTO detail_order VALUES("146","95","16","1","dia ajaa","Y");
INSERT INTO detail_order VALUES("147","103","16","2","yang davi","Y");
INSERT INTO detail_order VALUES("148","103","17","1","hehe","Y");
INSERT INTO detail_order VALUES("149","103","17","1","lili","Y");
INSERT INTO detail_order VALUES("150","103","19","1","lulululu","Y");
INSERT INTO detail_order VALUES("151","103","16","2","oooooo","Y");
INSERT INTO detail_order VALUES("152","104","19","1","jika","Y");
INSERT INTO detail_order VALUES("153","104","18","1","engkau","Y");
INSERT INTO detail_order VALUES("154","104","17","1","cinta","Y");
INSERT INTO detail_order VALUES("155","104","19","1","syang","Y");
INSERT INTO detail_order VALUES("156","105","16","1","Jangan Pedes","Y");
INSERT INTO detail_order VALUES("157","105","20","1","Jangan Terlalu Manis","Y");
INSERT INTO detail_order VALUES("158","105","18","1","Jangan Pedes","Y");
INSERT INTO detail_order VALUES("159","105","17","1","Jangan Pedes","Y");
INSERT INTO detail_order VALUES("160","106","17","1","Jangan Pedes","Y");
INSERT INTO detail_order VALUES("161","106","17","2","aku aja","Y");
INSERT INTO detail_order VALUES("162","106","16","1","hehehehe","Y");
INSERT INTO detail_order VALUES("163","106","18","1","hihihhii","Y");
INSERT INTO detail_order VALUES("164","106","19","1","","Y");
INSERT INTO detail_order VALUES("165","106","16","3","kikikiki","Y");
INSERT INTO detail_order VALUES("166","107","16","2","bubububuu","Y");
INSERT INTO detail_order VALUES("167","107","17","1","bibibibii","Y");



DROP TABLE kategori;

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(30) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO kategori VALUES("5","Minuman");
INSERT INTO kategori VALUES("6","Nasi");
INSERT INTO kategori VALUES("7","Mie");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","waiter");
INSERT INTO level VALUES("3","kasir");
INSERT INTO level VALUES("4","owner");
INSERT INTO level VALUES("5","pelanggan");



DROP TABLE masakan;

CREATE TABLE `masakan` (
  `id_masakan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_masakan` varchar(50) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `jenis` enum('Makanan','Minuman','','') NOT NULL,
  `harga` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status_masakan` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Habis, Y Tersedia',
  PRIMARY KEY (`id_masakan`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

INSERT INTO masakan VALUES("16","Mie Aceh","7","Makanan","15000","images_mie_Mie_Aceh_36-mie-aceh-1.jpg","Y");
INSERT INTO masakan VALUES("17","MIe Goreng","7","Makanan","6000","mie-goreng-beef-dan-sosis-foto-resep-utama.jpg","Y");
INSERT INTO masakan VALUES("18","Nasi Goreng","6","Makanan","15000","664xauto-cara-membuat-nasi-goreng-1810254.jpg","Y");
INSERT INTO masakan VALUES("19","Teh Manis","5","Minuman","5000","057982300_1422864990-mint-tea.jpg","Y");
INSERT INTO masakan VALUES("20","Jus Alpukat","5","Minuman","10000","images.jpeg","Y");
INSERT INTO masakan VALUES("24","Mie Tektek","7","Makanan","12000","664xauto-cara-membuat-nasi-goreng-1810254.jpg","Y");
INSERT INTO masakan VALUES("25","Nasi Goreng Ayam","6","Minuman","17000","664xauto-cara-membuat-nasi-goreng-1810254.jpg","Y");
INSERT INTO masakan VALUES("26","Nasi Goreng Baso","6","Makanan","17000","664xauto-cara-membuat-nasi-goreng-1810254.jpg","Y");
INSERT INTO masakan VALUES("27","Es Teh Manis","5","Minuman","5000","057982300_1422864990-mint-tea.jpg","Y");
INSERT INTO masakan VALUES("28","Mie Goreng Rendang","7","Makanan","7000","images_mie_Mie_Aceh_36-mie-aceh-1.jpg","Y");



DROP TABLE meja;

CREATE TABLE `meja` (
  `id_meja` int(11) NOT NULL AUTO_INCREMENT,
  `no_meja` varchar(15) NOT NULL,
  `status_meja` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_meja`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO meja VALUES("1","1","kosong");
INSERT INTO meja VALUES("2","2","kosong");
INSERT INTO meja VALUES("3","3","kosong");
INSERT INTO meja VALUES("4","4","kosong");
INSERT INTO meja VALUES("5","5","kosong");
INSERT INTO meja VALUES("6","6","penuh");
INSERT INTO meja VALUES("7","7","kosong");



DROP TABLE oder;

CREATE TABLE `oder` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `no_meja` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `status_order` varchar(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_order`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;

INSERT INTO oder VALUES("93","1","2019-03-09","22","","Y");
INSERT INTO oder VALUES("94","1","2019-03-09","22","","Y");
INSERT INTO oder VALUES("95","1","2019-03-09","2","","Y");
INSERT INTO oder VALUES("96","2","2019-03-09","21","","Y");
INSERT INTO oder VALUES("97","3","2019-03-09","2","","Y");
INSERT INTO oder VALUES("98","5","2019-03-09","2","","Y");
INSERT INTO oder VALUES("99","4","2019-03-19","22","","Y");
INSERT INTO oder VALUES("100","5","2019-03-19","22","","Y");
INSERT INTO oder VALUES("101","7","2019-03-19","22","","Y");
INSERT INTO oder VALUES("102","6","2019-03-19","22","","Y");
INSERT INTO oder VALUES("103","3","2019-03-23","22","","Y");
INSERT INTO oder VALUES("104","4","2019-03-23","22","","Y");
INSERT INTO oder VALUES("105","7","2019-03-23","22","","Y");
INSERT INTO oder VALUES("106","6","2019-03-23","22","","Y");
INSERT INTO oder VALUES("107","7","2019-03-29","22","","Y");



DROP TABLE recovery_keys;

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

INSERT INTO recovery_keys VALUES("1","1","94540c8b02feb18a4a522ce664cae561","1");
INSERT INTO recovery_keys VALUES("2","2","e436d7893a671386484ff33d110ea959","1");
INSERT INTO recovery_keys VALUES("3","1","cbc55f26f476d9183eb33156bd4c922b","0");
INSERT INTO recovery_keys VALUES("4","1","ae39191fab140225e8a1a56709f942ad","0");
INSERT INTO recovery_keys VALUES("5","1","10e3a403ad9d84fef2c21c4b491661fd","0");
INSERT INTO recovery_keys VALUES("6","1","de1167383d9a2abd34b44dcc38853a2a","0");
INSERT INTO recovery_keys VALUES("7","1","98c3ad70246159a7ae8cb2068c43bff3","0");
INSERT INTO recovery_keys VALUES("8","22","26834a753b67fd16cf909a46dff5d77f","1");
INSERT INTO recovery_keys VALUES("9","22","9aaca0b76db5edc2b7283f085aee223f","1");
INSERT INTO recovery_keys VALUES("10","22","d2b4fc2435977a730ac74d6e96928a17","1");
INSERT INTO recovery_keys VALUES("11","22","e25d4d645d9745f89b3df841c23cfbbf","1");
INSERT INTO recovery_keys VALUES("12","22","5a8f81261232e8c036733c1e43807d57","1");
INSERT INTO recovery_keys VALUES("13","22","cb79147c6c63f16985e4372ba33656cd","1");
INSERT INTO recovery_keys VALUES("14","22","e3e34dead22603b575d92ad61172ed4d","1");
INSERT INTO recovery_keys VALUES("15","22","b23abcd22f948480d8bd04275b969945","1");
INSERT INTO recovery_keys VALUES("16","22","ab25db167743ed4839e564ee8b408056","1");
INSERT INTO recovery_keys VALUES("17","22","e1e7b4c9db534b42de817da49c12c731","1");
INSERT INTO recovery_keys VALUES("18","22","6b0428fe24324286b8fd467f298f3e5d","1");
INSERT INTO recovery_keys VALUES("19","22","73332746584ae0dd0423a8f906626b11","1");
INSERT INTO recovery_keys VALUES("20","22","404b23fb92a842d68bac466b8dd826be","0");
INSERT INTO recovery_keys VALUES("21","22","42dafc607d4a97dc725874fda6f897f5","1");
INSERT INTO recovery_keys VALUES("22","22","451c3b791bc86cc40bfa55a25fce1e00","1");
INSERT INTO recovery_keys VALUES("23","22","daf76e15bfeb13330a95203d4b225c26","0");
INSERT INTO recovery_keys VALUES("24","22","650a4e757cbcad1acb3f0d578b938cfb","1");
INSERT INTO recovery_keys VALUES("25","22","8b7d7aa7e8a2d50cc6efd4a40b723727","1");
INSERT INTO recovery_keys VALUES("26","22","1153c31879555d9668ba9eaf1d72e44e","1");
INSERT INTO recovery_keys VALUES("27","22","e946b595eab6de99a94e7c7c126f8a0f","0");
INSERT INTO recovery_keys VALUES("28","22","26fa0e9fb0a71b7b0d48f282d7074a86","0");
INSERT INTO recovery_keys VALUES("29","22","ba0c0a1cd37d646c8ad0b743590fe882","1");
INSERT INTO recovery_keys VALUES("30","22","efb29428ef00230c660cec2696b77d5f","1");
INSERT INTO recovery_keys VALUES("31","22","59419c9454b25422291f991b7edf2865","1");
INSERT INTO recovery_keys VALUES("32","22","e511f489f88357070e1524a5924b40cf","1");
INSERT INTO recovery_keys VALUES("33","22","580f4750e0172f8c6cad264b97543821","1");



DROP TABLE transaksi;

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(12) NOT NULL,
  `keterangan_transaksi` varchar(10) NOT NULL,
  `jumlah_uang` int(11) NOT NULL,
  `kembalian` int(11) NOT NULL,
  PRIMARY KEY (`id_transaksi`),
  KEY `id_user` (`id_user`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

INSERT INTO transaksi VALUES("81","3","93","2019-03-09","27000","Y","64000","37000");
INSERT INTO transaksi VALUES("82","3","94","2019-03-09","10000","Y","12000","2000");
INSERT INTO transaksi VALUES("83","3","95","2019-03-29","15000","Y","58000","43000");
INSERT INTO transaksi VALUES("84","3","96","2019-03-09","15000","Y","17000","2000");
INSERT INTO transaksi VALUES("85","22","97","2019-03-09","5000","Y","7000","2000");
INSERT INTO transaksi VALUES("86","3","98","2019-03-09","7000","Y","8000","1000");
INSERT INTO transaksi VALUES("87","22","99","2019-03-19","5000","Y","6000","1000");
INSERT INTO transaksi VALUES("88","22","100","2019-03-19","5000","Y","7000","2000");
INSERT INTO transaksi VALUES("89","22","101","2019-03-19","15000","Y","17000","2000");
INSERT INTO transaksi VALUES("90","22","102","2019-03-19","30000","Y","30000","0");
INSERT INTO transaksi VALUES("91","22","103","2019-03-23","65000","Y","78000","13000");
INSERT INTO transaksi VALUES("92","22","104","2019-03-23","31000","Y","32000","1000");
INSERT INTO transaksi VALUES("93","22","105","2019-03-23","46000","Y","48000","2000");
INSERT INTO transaksi VALUES("94","0","106","0000-00-00","98000","N","0","0");
INSERT INTO transaksi VALUES("95","22","107","2019-03-29","36000","Y","40000","4000");



DROP TABLE user;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y aktif',
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

INSERT INTO user VALUES("2","waiter","202cb962ac59075b964b07152d234b70","waiter@gmail.com","waiter","Y","2");
INSERT INTO user VALUES("3","kasir","de28f8f7998f23ab4194b51a6029416f","asdasdasd@Dasdas.com","kasir","Y","3");
INSERT INTO user VALUES("4","owner","5be057accb25758101fa5eadbbd79503","owner@gmail.com","owner","Y","4");
INSERT INTO user VALUES("11","pelanggan","81448138f5f163ccdba4acc69819f280","dappp@gmail.com","dappp","Y","5");
INSERT INTO user VALUES("20","maulana","aff4b352312d5569903d88e0e68d3fbb","maulana@gmail.com","maulana fatullah","N","2");
INSERT INTO user VALUES("21","Budi","00dfc53ee86af02e742515cdcf075ed3","budi@gmail.com","Budi","Y","5");
INSERT INTO user VALUES("22","daviperdiansyah","250cf8b51c773f3f8dc8b4be867a9a02","daviperdiansyah44@gmail.com","Davi Perdiansyah","Y","1");
INSERT INTO user VALUES("23","kevine","202cb962ac59075b964b07152d234b70","kevine12@gmail.com","kevin atami tanos","Y","2");
INSERT INTO user VALUES("25","p","83878c91171338902e0fe0fb97a8c47a","p@gmail.com","p","N","5");
INSERT INTO user VALUES("26","admin","21232f297a57a5a743894a0e4a801fc3","daviperdiansyah44@gmail.com","Dav","Y","1");
INSERT INTO user VALUES("27","n","7b8b965ad4bca0e41ab51de7b31363a1","n@ygyf","n","N","5");
INSERT INTO user VALUES("28","i","865c0c0b4ab0e063e5caa3387c1a8741","i@gmail.com","i","Y","2");
INSERT INTO user VALUES("29","waiter1","151a6c018a98e6eff4b4387b0a438a44","sososos12@gmail.com","soso","Y","1");



