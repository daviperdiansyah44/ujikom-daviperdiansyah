<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php 
           include('include/body.php');
           ?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Order </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
               <div class="col-md-8">
                    <div class="white-box">
           <?php
          include '../koneksi.php';
           $data_ambil_id_transaksi = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM transaksi where id_order='$_GET[id_order]'"));
 
          ?>
          <div class="box-body">
           <?php
                $a = mysqli_query($conn,"SELECT keterangan_transaksi FROM transaksi WHERE id_order='$_GET[id_order]'");
                $b = mysqli_fetch_array($a);
                if($b[0]=="N"){
             ?>
             <div class="agile3-grids">
             <button class="btn btn-success" type="submit" data-toggle="modal"
                                                    data-target="#myModal<?php echo $data_ambil_id_transaksi['id_transaksi'];?>"><i class="fa fa-shopping-cart"></i> Bayar
            </button>
            <?php }?>
            
            <button class="btn btn-danger" type="submit" ><a href="transaksi"><font color="white">Kembali</font></a>
            </button>
          </div>
          </div>
        </br>
<div class="table-responsive">

       <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Masakan</th>
                  <th>Harga</th>
                    <th>Quantity</th>
                   <th>Keterangan</th>
                   <th>Status</th>
                   <th>Jumlah</th>
                   
                 

                </tr>
              </thead>
              <?php
              error_reporting(0);
              $no = 1;
              $total=0;
             
              foreach($db->detail_tampil() as $x){
               $harga=$x['harga'];
                $jumlah=$x['jumlah']*$x['harga'];
                  $hasil="Rp.".number_format($harga,2,',','.');
                   $jml=$x['jml'] *$harga;
                    $jumlah1="Rp.".number_format($jml,2,',','.');
                ?>
                <tbody>

                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $x['nama_masakan']; ?></td>
                    <td><?php echo $hasil; ?></td>
                    <td><?php echo $x['jml']; ?></td>
                    <td><?php echo $x['keterangan']; ?></td>
                    <td><?php
                                            if($x['status_detail_order'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Sudah di Terima";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum di Terima";?>
                                            <?php 
                                            }
                                            ?></td>
                    <td><?php echo $jumlah1;?></td>

                    

                  </tr>
                 

                </tbody>
                <?php 
                $total += ($jml) ;
                 $total1="Rp.".number_format($total,2,',','.');
              }
              ?>
                                         <tr>


  <td colspan="6" align="right">Total</td>
  <td ><?php echo $total1;?></td>
 
  </tr>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
      <div class="col-md-4">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="white-box">
             <?php
             include "../koneksi.php";
$query_edit = mysqli_query($conn,"SELECT *,oder.tanggal,user.id_user FROM oder inner join user on oder.id_user=user.id_user inner join transaksi on oder.id_order=transaksi.id_order where oder.id_order='$_GET[id_order]'");
$a = mysqli_fetch_array($query_edit)
?>
                  <h3 class="box-title">Data Order NO Meja <?php echo $a['no_meja']; ?></h3><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                  <div class="form-group">
                      <label for="exampleInputEmail1">ID Order</label>
                      <input class="form-control" value="<?php echo $a['id_order']; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">No Meja</label>
                      <input class="form-control" value="<?php echo $a['no_meja']; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Pembeli</label>
                      <input  class="form-control" value="<?php echo $a['nama_user']; ?>" readonly>
                    </div>
                         <div class="form-group">
                      <label for="exampleInputEmail1">Tanggal</label>
                      <input class="form-control" value="<?php echo $a['tanggal']; ?>" readonly>
                    </div>
                      <div class="form-group">
                      <label for="exampleInputEmail1">Keterangan Pembayaran</label>
                    <?php
                                            if($a['keterangan_transaksi'] == 'Y')
                                            {
                                              ?>
                                         
                      <input class="form-control" value="<?php echo "Terbayar"; ?>" readonly>
                    
                                          
                                            <?php
                                            }else{
                                              ?>
                                           
                     
                      <input class="form-control" value="<?php echo "Belum Terbayar"; ?>" readonly>
                    </div>
                                            <?php 
            }
            ?>
                     
                  </div><!-- /.box-body -->
                </form>
                </div>
              </div><!-- /.box -->
            </div>
       
                 <div class="modal fade" id="myModal<?php echo $data_ambil_id_transaksi['id_transaksi'];?>" tabindex="-1" role="dialog"
                                             aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">&times;
                                                        </button>
                                                      
                                                        <h4 class="modal-title"> TOTAL :
                                                             <?php echo $total1; ?></h4>
                                                             
                                                    </div>

                                                    <div class="modal-body row">
                                                        <div class="col-md-12">
                                                            <!--<form method="POST" action="?hal=cetak">-->
                                                                <form method="POST" action="update_transaksi.php?id_transaksi=<?php echo $data_ambil_id_transaksi['id_transaksi'];?>">
                                                                <div class="form-group">
                                                                    <label> Chash</label>

                                                               <input type="hidden" class="form-control" value="<?php echo $a['no_meja']; ?>" name="id_meja"/>
                                                               <input type="text" class="form-control" value="<?php echo $a['id_user']; ?>" name="id_user"/>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="type1" name="jumlah_uang"/>
                                                                </div>

                                                                <div class="pull-right">
                                                                    <button class="btn btn-primary btn-sm"
                                                                            type="submit"><i
                                                                                class="fa fa-check-square-o"></i> OK
                                                                    </button>
                                                                    <button class="btn btn-danger btn-sm"
                                                                            data-dismiss="modal" aria-hidden="true"
                                                                            type="button"><i class="fa fa-times"></i>
                                                                        Cancel
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


               </tr>
                                        </tr>
                                    </tbody>
                                   </table>
                            </div>
                    </form>
                  </div>
                </div>


        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
