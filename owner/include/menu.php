<ul class="nav nav-pills nav-stacked custom-nav">
                <li class=""><a href="index"><i class="icon-home"></i> <span>Dashboard</span></a>
                </li>
                <li class="menu-list"><a href="#"><i class="fa fa-print"></i> <span>Generata Laporan</span></a>
                    <ul class="sub-menu-list" style="background: #202427;">
                        <li><a href="laporan">Laporan Transaksi</a></li>
                        <li><a href="laporan_data_order">Laporan Data Order</a></li>
                        <li><a href="pengguna">Laporan Data Pengguna</a></li>
                        <li><a href="data_makanan">Laporan Data Masakan</a></li>
                    </ul>
                </li>
            </ul>