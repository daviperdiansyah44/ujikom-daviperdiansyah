<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../admin/index");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'../admin/database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php include("include/body.php");?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Pengguna </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                       <a href="lap_user" class="btn btn-danger" target="blank">Export ke PDF</a><br><br>
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Nama User</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Level</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                         <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Nama User</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Level</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    foreach($db->tampil_data() as $x){
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['username']; ?></td>
                                            <td><?php echo $x['nama_user']; ?></td>
                                            <td><?php echo $x['email']; ?></td>
                                            <td>
                                            <?php
                                            if($x['status'] == 'Y')
                                            {
                                              ?>
                                            <a href="approve?table=user&id_user=<?php echo $x['id_user']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Aktif
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approve?table=user&id_user=<?php echo $x['id_user']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Tidak Aktif
                                            </a>
                                            <?php
                                            }
                                            ?>
                                            </td>
                                            <td><?php echo $x['nama_level']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                   </table>
                            </div>
                       </div>
                   </div>
               </div>
        <!-- End Wrapper-->
        </div>

        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
