<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../admin/index");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'../admin/database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php include("include/body.php");?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php 
           include('include/menu.php');
           ?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Makanan dan Minuman </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
        </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                       <div class="box-header">
                  <a href="lap_makanan" class="btn btn-danger" target="blank">Export ke PDF</a><br><br>
                </div><br>
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                     <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Masakan</th>
                        <th>Jenis</th>
                        <th>Kategori</th>
                        <th>Gambar</th>
                        <th>Harga</th>
                        <th>Status Masakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Nama Masakan</th>
                        <th>Jenis</th>
                        <th>Kategori</th>
                        <th>Gambar</th>
                        <th>Harga</th>
                        <th>Status Masakan</th>
                      </tr>
                    </tfoot>
                    <tbody>
<?php
include "../koneksi.php";
$no=0;
$data = "SELECT * from masakan inner join kategori on masakan.id_kategori = kategori.id_kategori";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
$no++;
$nama_masakan        = $select_result['nama_masakan'];
$jenis               = $select_result['jenis'];
$kategori            = $select_result['nama_kategori'];
$gambar              = $select_result['image'];
$harga               = $select_result['harga'];
$status_masakan      = $select_result['status_masakan'];

echo"<tr>
<td>$no</td> 
<td>$nama_masakan</td>
<td>$jenis</td>
<td>$kategori</td>
<td><img src='../admin/gambar/$gambar' height='100'></td>
<td>$harga</td>
";
//ganti imagesup dengan nama folder dimana anda menempatkan image hasil upload
?>
<td>                                        <?php
                                            if($select_result['status_masakan'] == 'Y')
                                            {
                                              ?>
                                            <a href="approve_makanan?table=masakan&id_masakan=<?php echo $select_result['id_masakan']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Tersedia
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                            <a href="approve_makanan?table=masakan&id_masakan=<?php echo $select_result['id_masakan']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Habis
                                            </a>
                                            <?php
                                            }
                                            ?></td>
                                            </tr>
<?php }  ?>
                    </tbody>
                  </table>
        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
