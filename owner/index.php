<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../admin/index");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../assets/logo.png" type="image/png">
  <title>Owner - D'Resto</title>

    <!--Begin  Page Level  CSS -->
    <script type="text/javascript" src="../admin/chartjs/Chart.js"></script>
    <link href="assets/plugins/morris-chart/morris.css" rel="stylesheet">
    <link href="assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
     <!--End  Page Level  CSS -->
    <link href="assets/css/icons.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="js/html5shiv.min.js"></script>
          <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <div class="logo">
            <a href="index"><img width="55%" src="../admin/assets/images/owner1.png" alt=""></a>
        </div><br>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class=""><a href="index"><i class="icon-home"></i> <span>Dashboard</span></a>
                </li>
                <li class="menu-list"><a href="#"><i class="fa fa-print"></i> <span>Generata Laporan</span></a>
                    <ul class="sub-menu-list" style="background: #202427;">
                        <li><a href="laporan">Laporan Transaksi</a></li>
                        <li><a href="laporan_data_order">Laporan Data Order</a></li>
                        <li><a href="pengguna">Laporan Data Pengguna</a></li>
                        <li><a href="data_makanan">Laporan Data Masakan</a></li>
                    </ul>
                </li>
            </ul>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
              
          <!--Start Page Title-->
           <div class="page-title-box">
                <h4 class="page-title">Dashboard </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
              <!--End Page Title-->          
           
                 <!--Start row-->
                 <div class="row">
                     <div class="container">
                         <div class="analytics-box">
                             
                         </div>
                     </div>
                 </div>
                 <!--End row-->
           
                  <!--Start row-->
                  <div class="row">
                   <!--Start info box-->
                   <div class="col-md-3 col-sm-6">
                       <div class="info-box-main">
                          <div class="info-stats">
                          <?php
                          include '../koneksi.php';
                          $jumlah = 0;
                          $sql = mysqli_query($conn, "SELECT * FROM user INNER JOIN level on user.id_level = level.id_level where nama_level ='pelanggan'");
                          while($row = mysqli_fetch_array($sql)){
                            $jumlah++;
                          }
                              echo "<p>$jumlah</p>"
                          ?>
                              <span>Jumlah Pelanggan</span>
                          </div>
                          <div class="info-icon text-primary ">
                              <i class="fa fa-user"></i>
                          </div>
                          <div class="info-box-progress">
                             <div class="progress">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100" style="width: 48%;">
                             </div>
                          </div>
                          </div>
                       </div>
                   </div>
                   <!--End info box-->
                   
                   <!--Start info box-->
                   <div class="col-md-3 col-sm-6">
                       <div class="info-box-main">
                          <div class="info-stats">
                           <?php
                          include '../koneksi.php';
                          $jumlah = 0;
                          $sql = mysqli_query($conn, "SELECT * FROM user INNER JOIN level on user.id_level = level.id_level where nama_level ='waiter'");
                          while($row = mysqli_fetch_array($sql)){
                            $jumlah++;
                          }
                              echo "<p>$jumlah</p>"
                          ?>
                              <span>Jumlah Waiter</span>
                          </div>
                          <div class="info-icon text-info">
                             <i class="fa fa-cutlery"></i>	
                          </div>
                          <div class="info-box-progress">
                             <div class="progress">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100" style="width: 48%;">
                             </div>
                          </div>
                          </div>
                       </div>
                   </div>
                   <!--End info box-->

                    <!--Start info box-->
                   <div class="col-md-3 col-sm-6">
                       <div class="info-box-main">
                          <div class="info-stats">
                           <?php
                          include '../koneksi.php';
                          $jumlah = 0;
                          $sql = mysqli_query($conn, "SELECT * FROM kategori");
                          while($row = mysqli_fetch_array($sql)){
                            $jumlah++;
                          }
                              echo "<p>$jumlah</p>"
                          ?>
                              <span>Menu Kategori</span>
                          </div>
                          <div class="info-icon text-danger">
                              <i class="fa fa-dollar"></i>
                          </div>
                          <div class="info-box-progress">
                             <div class="progress">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100" style="width: 48%;">
                             </div>
                          </div>
                          </div>
                       </div>
                   </div>
                   <!--End info box-->
                
                
                   <!--Start info box-->
                   <div class="col-md-3 col-sm-6">
                       <div class="info-box-main">
                          <div class="info-stats">
                           <?php
                          include '../koneksi.php';
                          $jumlah = 0;
                          $sql = mysqli_query($conn, "SELECT * FROM transaksi");
                          while($row = mysqli_fetch_array($sql)){
                            $jumlah++;
                          }
                              echo "<p>$jumlah</p>"
                          ?>
                              <span>Transaksi</span>
                          </div>
                          <div class="info-icon text-danger">
                              <i class="fa fa-dollar"></i>
                          </div>
                          <div class="info-box-progress">
                             <div class="progress">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100" style="width: 48%;">
                             </div>
                          </div>
                          </div>
                       </div>
                   </div>
                   <!--End info box-->
                
                  </div>
                  <!--End row-->
                  
                             
                  
                <!--Start row-->  
                 <?php
$conn = mysqli_connect("localhost" ,"root" ,"" ,"kasir");
  $sqljanuari = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 1 ORDER BY tanggal ASC");
  $januari = mysqli_fetch_array($sqljanuari);

  $sqlfebruari = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 2 ORDER BY tanggal ASC");
  $februari = mysqli_fetch_array($sqlfebruari);

  $sqlmaret = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 3 ORDER BY tanggal ASC");
  $maret = mysqli_fetch_array($sqlmaret);

  $sqlapril = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 4 ORDER BY tanggal ASC");
  $april = mysqli_fetch_array($sqlapril);

  $sqlmei = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 5 ORDER BY tanggal ASC");
  $mei = mysqli_fetch_array($sqlmei);

  $sqljuni = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 6 ORDER BY tanggal ASC");
  $juni = mysqli_fetch_array($sqljuni);

  $sqljuli = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 7 ORDER BY tanggal ASC");
  $juli = mysqli_fetch_array($sqljuli);

  $sqlagustus = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 8 ORDER BY tanggal ASC");
  $agustus = mysqli_fetch_array($sqlagustus);

  $sqlseptember = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 9 ORDER BY tanggal ASC");
  $september = mysqli_fetch_array($sqlseptember);

  $sqloktober = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 10 ORDER BY tanggal ASC");
  $oktober = mysqli_fetch_array($sqloktober);

  $sqlnovember = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 11 ORDER BY tanggal ASC");
  $november = mysqli_fetch_array($sqlnovember);

  $sqldesember = mysqli_query($conn,"SELECT COUNT(tanggal) FROM transaksi WHERE MONTH(tanggal) = 12 ORDER BY tanggal ASC");
  $desember = mysqli_fetch_array($sqldesember);
?>
                  
                <!--Start row-->  
                <style type="text/css">
    body{
      font-family: roboto;
    }
  </style>
 
  <h2></h2>
 <div class="table-responsive">
    <canvas id="myChart"></canvas>
 
  <script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Juni", "Juli", "Agust", "Sept", "Okt", "Nov", "Des"],
        datasets: [{
          label: 'Jumlah Transaki(kali)',
          data: [<?= $januari[0] ?>, <?= $februari[0] ?>, <?= $maret[0] ?>, <?= $april[0] ?>, <?= $mei[0] ?>, <?= $juni[0] ?>,<?= $juli[0] ?>,<?= $agustus[0] ?>,<?= $september[0] ?>,<?= $oktober[0] ?>,<?= $november[0] ?>,<?= $desember[0] ?>],
          backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)'
          ],
          borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      }
    });
  </script>
        <!-- End Wrapper-->
</div>
</div>
        <!-- End Wrapper-->


        <!--Start  Footer -->
        <footer class="footer-main"> 2017 &copy; Meter admin Template.	</footer>	
         <!--End footer -->

       </div>
      <!--End main content -->
    


    <!--Begin core plugin -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/plugins/moment/moment.js"></script>
    <script  src="assets/js/jquery.slimscroll.js "></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/functions.js"></script>
    <!-- End core plugin -->
    
    <!--Begin Page Level Plugin-->
	<script src="assets/plugins/morris-chart/morris.js"></script>
    <script src="assets/plugins/morris-chart/raphael-min.js"></script>
    <script src="assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="assets/pages/dashboard.js"></script>
    <!--End Page Level Plugin-->
   

</body>

</html>
