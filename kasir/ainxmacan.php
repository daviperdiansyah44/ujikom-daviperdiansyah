<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../admin/index");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'../admin/database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <div class="logo">
            <a href="index"><img src="assets/images/logo.png" alt=""></a>
        </div>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
             <ul class="nav nav-pills nav-stacked custom-nav">
                <li class=""><a href="index"><i class="icon-home"></i> <span>Dashboard</span></a>
                </li>

                <li class="menu-list"><a href="#"><i class="icon-layers"></i> <span>Master Data</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="pelanggan">Pelanggan</a></li>
                    </ul>
                </li>
                <li class=""><a href="transaksi"><i class="fa fa-money"></i> <span>Transaksi</span></a>
                <li class=""><a href="laporan"><i class="fa fa-print"></i> <span>Laporan</span></a>
                </li>
            </ul>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-wrench"></i> Settings </a> </li>
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="#"> <i class="fa fa-info"></i> Help </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Order </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No. Meja</th>
                                            <th>Tanggal</th>
                                            <th>Nama User</th>
                                            <th>Keterangan</th>
                                            <th>Status Order</th>
                                            <th>Keterangan Pembayaran</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                         <tr>
                                            <th>No</th>
                                            <th>No. Meja</th>
                                            <th>Tanggal</th>
                                            <th>Nama User</th>
                                            <th>Keterangan</th>
                                            <th>Status Order</th>
                                            <th>Keterangan Pembayaran</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    error_reporting(0);
                                    $no = 1;
                                    foreach($db->tampil_pesan_keterangan() as $x){
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['no_meja']; ?></td>
                                            <td><?php echo $x['tanggal']; ?></td>
                                            <td><?php echo $x['nama_user']; ?></td>
                                            <td><?php echo $x['keterangan']; ?></td>
                                            <td><?php
                                            if($x['status_order'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Sudah diterima";?></td><?php }?>
                                            <td><?php
                                            if($x['keterangan_transaksi'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Terbayar";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum Terbayar";?>
                                            <?php 
                                            }
                                            ?></td>
                                           <td>
                                           <button class="btn btn-success" type="submit" data-toggle="modal"
                                                                            data-target="#myModal<?php echo $x['id_transaksi'];?>"><i class="fa fa-shopping-cart"></i> Bayar
                                                                    </button>
                                           <a href="detail_bayar?id_order=<?php echo $x['id_order']; ?>"><button type="button" class="btn btn-warning">Detail</button></a>
                                             
                                         </td>

                        <div class="modal fade" id="myModal<?php echo $x['id_transaksi'];?>" tabindex="-1" role="dialog"
                                             aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">&times;
                                                        </button>
                                  
                                                        <h4 class="modal-title"> TOTAL :
                                                            Rp. <?php echo number_format($x['total_bayar'], 0, ',', '.'); ?></h4>
                                                    </div>

                                                    <div class="modal-body row">
                                                        <div class="col-md-12">
                                                            <!--<form method="POST" action="?hal=cetak">-->
                                                                <form method="POST" action="update_transaksi?id_transaksi=<?php echo $x['id_transaksi'];?>">
                                                                <div class="form-group">
                                                                    <label> Cash</label>
                                                               <input type="hidden" class="form-control" value="<?php echo $x['no_meja']; ?>" name="id_meja"/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="type1" name="jumlah_uang"/>
                                                                </div>

                                                                <div class="pull-right">
                                                                    <button class="btn btn-primary btn-sm"
                                                                            type="submit"><i
                                                                                class="fa fa-check-square-o"></i> OK
                                                                    </button>
                                                                    <button class="btn btn-danger btn-sm"
                                                                            data-dismiss="modal" aria-hidden="true"
                                                                            type="button"><i class="fa fa-times"></i>
                                                                        Cancel
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


               </tr>
                 <?php 
           }
           ?>
                                        </tr>
                                    </tbody>
                                   </table>
                            </div>
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
