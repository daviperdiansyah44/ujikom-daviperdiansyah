DROP TABLE detail_order;

CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_detail_order` enum('selesai','di proses') NOT NULL,
  PRIMARY KEY (`id_detail_order`),
  KEY `id_order` (`id_order`),
  KEY `id_masakan` (`id_masakan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","waiter");
INSERT INTO level VALUES("3","kasir");
INSERT INTO level VALUES("4","owner");
INSERT INTO level VALUES("5","pelanggan");



DROP TABLE masakan;

CREATE TABLE `masakan` (
  `id_masakan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_masakan` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status_masakan` enum('tersedia','habis') NOT NULL,
  PRIMARY KEY (`id_masakan`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO masakan VALUES("6","mie goreng","5000","gambar/IMG-20161025-WA0006.jpg","habis");



DROP TABLE oder;

CREATE TABLE `oder` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `no_meja` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `status_order` enum('sedang di pesan','sedang di masak','sedang di anter') NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE recovery_keys;

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO recovery_keys VALUES("1","1","94540c8b02feb18a4a522ce664cae561","1");
INSERT INTO recovery_keys VALUES("2","2","e436d7893a671386484ff33d110ea959","1");
INSERT INTO recovery_keys VALUES("3","1","cbc55f26f476d9183eb33156bd4c922b","0");
INSERT INTO recovery_keys VALUES("4","1","ae39191fab140225e8a1a56709f942ad","0");
INSERT INTO recovery_keys VALUES("5","1","10e3a403ad9d84fef2c21c4b491661fd","0");
INSERT INTO recovery_keys VALUES("6","1","de1167383d9a2abd34b44dcc38853a2a","0");



DROP TABLE transaksi;

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(12) NOT NULL,
  PRIMARY KEY (`id_transaksi`),
  KEY `id_user` (`id_user`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE user;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO user VALUES("1","admin","01cfcd4f6b8770febfb40cb906715822","daviperdiansyah44@gmail.com","administrator","1");
INSERT INTO user VALUES("2","waiter","wjwbdffk","","waiter","2");
INSERT INTO user VALUES("3","kasir","de28f8f7998f23ab4194b51a6029416f","","kasir","3");
INSERT INTO user VALUES("4","owner","5be057accb25758101fa5eadbbd79503","","owner","4");
INSERT INTO user VALUES("11","dappp","6fedff3bab0e74fa675979b1857ebafe","dappp@gmail.com","dappp","5");



