<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../admin/index");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php include("include/body.php");?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php include("include/menu.php");?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Pelanggan </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                       <a href="#usertambah" class="btn btn-success" data-toggle="modal">Tambah Pelanggan</a><br><br>
                       <a href="lap_user" class="btn btn-danger" target="blank">Export ke PDF</a><br><br>
                       <div class="modal" id="usertambah">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Tambah Pelanggan</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses?aksi=tambah" enctype="multipart/form-data" class="form-horizontal form-material"">
                        <div class="box-body">
                            <div class="form-group">
                              <label for="nama_user">Username</label>
                              <input type="text" class="form-control" name="username" id="username" placeholder="Masukan Username" required>
                            </div>
                            <div class="form-group">
                              <label for="harga">Paswword</label>
                              <input type="password" class="form-control" name="password" id="password" placeholder="Masukan Paswword" required>
                            </div>
                                <div class="form-group">
                                    <label for="email">Email :</label>
                                      <input type="email" id="email" class="form-control" placeholder="Masukkan Email" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="nama_user">Nama User :</label>
                                      <input type="text" id="nama_user" class="form-control" placeholder="Masukkan Nama Anda" name="nama_user">
                                </div>
                               <div class="form-group">
                                    <label for="id_level">Nama Level :</label>
                                    <select name="id_level">
                                       <option>Pilih Nama Level</option>
                                          <?php     
                                            include"../koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM level where id_level = '5'");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                                      <option value="<?=$show['id_level'];?>"><?=$show['nama_level'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Nama User</th>
                                            <th>Status</th>
                                            <th>Level</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                         <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Nama User</th>
                                            <th>Status</th>
                                            <th>Level</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    foreach($db->tampil_data() as $x){
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['username']; ?></td>
                                            <td><?php echo $x['nama_user']; ?></td>
                                            <td>
                                            <?php
                                            if($x['status'] == 'Y')
                                            {
                                              ?>
                                            <a href="approve?table=user&id_user=<?php echo $x['id_user']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Aktif
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approve?table=user&id_user=<?php echo $x['id_user']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Tidak Aktif
                                            </a>
                                            <?php
                                            }
                                            ?>
                                            </td>
                                            <td><?php echo $x['nama_level']; ?></td>
                                        </tr>
                                    </tbody>
                                    <?php } ?>
                                   </table>
                            </div>
                            <?php
include "../koneksi.php";
$no=0;
$data = "SELECT * from user";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
  ?>
  <?php
$id = $select_result['id_user']; 
$query_edit = mysqli_query($conn,"SELECT * FROM user WHERE id_user='$id'");
$r = mysqli_fetch_array($query_edit);
?>
            <div class="modal" id="myModal<?php echo $select_result['id_user'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Menu</h4>
                  </div>

                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses?aksi=update" enctype="multipart/form-data" class="form-horizontal form-material"">
                            <div class="form-group">
                                    <label for="username">Username :</label>
                                      <input type="hidden" name="id_user" value="<?php echo $r['id_user']?>">
                                      <input type="text" id="username" class="form-control" placeholder="Masukkan Username" name="username" value="<?php echo $r['username']?>">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password :</label>
                                      <input type="password" name="password" id="password" class="form-control" placeholder="">
                                      <input type="hidden" name="password_lama" placeholder="" value="<?php echo $r['password'] ?>">                                </div>
                                <div class="form-group">
                                    <label for="email">Email :</label>
                                      <input type="email" id="email" class="form-control" placeholder="Masukkan Email yang Falid" name="email" value="<?php echo $r['email']?>">
                                </div>
                                <div class="form-group">
                                    <label for="nama_user">Nama User :</label>
                                      <input type="text" id="nama_user" class="form-control" placeholder="Masukkan Nama Anda" name="nama_user" value="<?php echo $r['nama_user']?>">
                                </div>
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
<?php } ?>
                       </div>
                   </div>
               </div>
        <!-- End Wrapper-->
        </div>

        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
