<ul class="nav nav-pills nav-stacked custom-nav">
                <li class=""><a href="index"><i class="icon-home"></i> <span>Dashboard</span></a>
                </li>

                <li class="menu-list"><a href="#"><i class="icon-layers"></i> <span>Master Data</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="pelanggan">Pelanggan</a></li>
                        <li><a href="data_terbayar">Data Transaksi</a></li>
                    </ul>
                </li>
                <li class=""><a href="transaksi"><i class="fa fa-money"></i> <span>Transaksi</span></a>
                </li>
                 <li class="menu-list"><a href="#"><i class="fa fa-print"></i> <span>Laporan</span></a>
                    <ul class="sub-menu-list" style="background: #202427;">
                        <li><a href="laporan">Generate Laporan</a></li>
                    </ul>
                </li>
            </ul>