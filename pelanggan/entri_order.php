<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>D'Resto</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="../assets/logo.png" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">    
    <!-- Date Picker -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-datepicker.css">   
     <!-- Gallery Lightbox -->
    <link href="assets/css/magnific-popup.css" rel="stylesheet"> 
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">     
    
    <!-- Main style sheet -->
    <link href="style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->

    <!-- Prata for body  -->
    <link href='https://fonts.googleapis.com/css?family=Prata' rel='stylesheet' type='text/css'>
    <!-- Tangerine for small title -->
    <link href='https://fonts.googleapis.com/css?family=Tangerine' rel='stylesheet' type='text/css'>   
    <!-- Open Sans for title -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

  <!--START SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#">
      <i class="fa fa-angle-up"></i>
    </a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header section -->
  <header id="mu-header">  
    <nav class="navbar navbar-default mu-main-navbar" role="navigation">  
      <div class="container">
        <div class="navbar-header">
          <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <!-- LOGO -->       

           <!--  Text based logo  -->
         <a class="navbar-brand" href="index"><img style="margin-top: -35px;" width="85%" src="../admin/assets/pdf/logo.png"></a>  

		      <!--  Image based logo  -->
          <!-- <a class="navbar-brand" href="index.html"><img src="assets/img/logo.png" alt="Logo img"></a>  -->
         

        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul id="top-menu" class="nav navbar-nav navbar-right mu-main-nav">
            <li><a href="index">HOME</a></li>
            <li><a href="logout">KELUAR</a></li>
            <li><a href="entri_order"><img style="width: 30px;" height="30px;" src="../waiter/cart.png"></a></li>
            <span class="badge">
                <?php
                if(isset($_SESSION['items'])){
                  echo count($_SESSION['items']);
                }
                else{
                  echo "0";
                }
                ?>
        </span>
        </div><!--/.nav-collapse -->       
      </div>          
    </nav> 
  </header><br><br>
  <!-- End header section -->

  <!-- Start Restaurant Menu -->
  <section id="mu-restaurant-menu">
    <div class="container">
      <div class="row">
      <div class="table-responsive">
        <div class="col-md-12">
          <div class="mu-restaurant-menu-area">

            <div class="mu-title">
              <span class="mu-subtitle">Tabel Masakan Anda</span><br>
              <p align="left"><a href="index"><button class="btn btn-success" type="submit"><i class="fa fa-shopping-cart">&nbsp;Tambah Pesanan</i></button></a></p>
            </div>
            <form action="proses_masakan" method="post">
              <tr>
              <table class="table table-hover table-condensed">
                    <th><center>No</center></th>
                    <th><center>Nama Masakan</center></th>
                    <th><center>Harga</center></th>
                    <th><center>Quantity</center></th>
                    <th><center>Sub Total</center></th>
                    <th><center>Keterangan</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
    <?php
    include"../koneksi.php";
        //MENAMPILKAN DETAIL KERANJANG BELANJA//
    $no = 1;
    $total = 0;
    //mysql_select_db($database_conn, $conn);
    if (isset($_SESSION['items'])) {
        foreach ($_SESSION['items'] as $key => $val) {
            $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
            $data = mysqli_fetch_array($query);
      $jumlah_barang = mysqli_num_rows($query);
            $jumlah_harga = $data['harga'] * $val;
            $total += $jumlah_harga;
            $harga = $data['harga'];
            $hasil = "Rp.".number_format($harga,2,',','.');
            $hasil1 = "Rp.".number_format($jumlah_harga,2,',','.');
            ?>
                <tr>
                <td><center><?php echo $no++; ?></center></td>
                <td><center><input type="hidden" name="id_masakan[]" value="<?php echo $data['id_masakan']; ?>"><?php echo $data['nama_masakan'];?></center></td>
                <td><center><?php echo $hasil;?></center></td>
                <td><center><a href="cart?act=plus&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=entri_order" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></a>
                <input type="hidden" name="jumlah[]" value="<?php echo ($val);?>"><?php echo ($val);?>Pcs<a href="cart?act=min&amp;id_masakan=<?php echo $data['id_masakan'];?> &amp;ref=entri_order" class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></a>
                </center></td>
                <td><center><?php echo $hasil1;?></center></td>
                <td><center><textarea class="form-control" name="keterangan[]"></textarea></center></td>
                <td><center>
                <a href="cart?act=del&amp;id_masakan=<?php echo $data ['id_masakan'];?> &amp;ref=entri_order">Batal</a>
                </center></td>
                </tr>
                

          <?php
                    //mysql_free_result($query);      
            }
              //$total += $sub;
            }?>
                        <?php
        if($total == 0){ ?>
          <td colspan="12" align="center"><?php echo "Tabel Masakan Anda Kosong!"; ?></td>
        <?php } else { ?>
                        <td colspan="8" style="font-size: 18px;"><b><div class="pull-right"><input type="hidden" value="<?php echo $total;?>" name="total_bayar"> Total Harga Anda : Rp. <?php echo number_format($total); ?>,- </div> </b></td>
          
        <?php 
        
        }
        ?>
                </table> 
              <p align='right'>
             <a href="#tambahuser" data-toggle="modal" class="btn btn-primary">Pesan</a>
                <div class="modal" id="tambahuser">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Pelanggan</h4>
                  </div>
                  <div class="modal-body">
                    
                        <div class="box-body">
                            
                        

                            <div class="form-group">
                                    <label for="no_meja">No Meja :</label>
                                    <select name="no_meja" class="form-control" required>
                                       <option>Pilih No Meja</option>
                                      <?php
                                      include'../koneksi.php';
                                        $query = $conn->query("SELECT * FROM meja WHERE status_meja = 'kosong'");
                                        $hitung = mysqli_num_rows($query);
                                        while($rows = mysqli_fetch_assoc($query)):
                                      ?>
                                        <option value="<?php echo $rows['id_meja'];?>"><?php echo $rows['no_meja'];?></option>
                                      <?php endwhile;?>
                                  </select>
                                </div>
                         
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                      <div class="form-group">
                      <?php
              if ($hitung  < 1) {
                echo"<div class='alert alert-danger' align='center'>Mohon maaf meja sedang penuh</div>";
              }
              else{
            ?>
                    <?php }?>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success"> Simpan</button>
                    </div>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
     </form>
      

   </div><!-- /.box-body -->


    </div><!-- /.box -->
</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>
  <!-- End Restaurant Menu -->

  <!-- Start Footer -->
  <footer id="mu-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <div class="mu-footer-area">
           <div class="mu-footer-social">
            <a href="#"><span class="fa fa-facebook"></span></a>
            <a href="#"><span class="fa fa-twitter"></span></a>
            <a href="#"><span class="fa fa-google-plus"></span></a>
            <a href="#"><span class="fa fa-linkedin"></span></a>
            <a href="#"><span class="fa fa-youtube"></span></a>
          </div>
          <div class="mu-footer-copyright">
            <p>&copy; Copyright <a rel="nofollow" href="http://markups.io">markups.io</a>. All right reserved.</p>
          </div>         
        </div>
      </div>
      </div>
    </div>
  </footer>
  <!-- End Footer -->
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/simple-animated-counter.js"></script>
  <!-- Gallery Lightbox -->
  <script type="text/javascript" src="assets/js/jquery.magnific-popup.min.js"></script>
  <!-- Date Picker -->
  <script type="text/javascript" src="assets/js/bootstrap-datepicker.js"></script> 
  <!-- Ajax contact form  -->
  <script type="text/javascript" src="assets/js/app.js"></script>
 
  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>