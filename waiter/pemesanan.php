<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../admin/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../admin/assets/pdf/logo.png" type="image/png">
  <title>Waiter - D'Resto</title>

    <!--Begin  Page Level  CSS -->
    <link href="assets/plugins/morris-chart/morris.css" rel="stylesheet">
    <link href="assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
     <!--End  Page Level  CSS -->
    <link href="assets/css/icons.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="js/html5shiv.min.js"></script>
          <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <?php include("include/body.php");?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
           <?php include('include/menu.php');?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Entry Order </h4>
                <ol class="breadcrumb">
                </ol>
                <div class="clearfix"></div>
             </div>
             <a href="detail_masakan"><img width="30" src ="cart.png"></a>
        <span class="badge">
        <?php
        if(isset($_SESSION['items'])){
          echo count($_SESSION['items']);
        }
        else{
          echo "0";
        }
        ?>
        </span><br>
        <h4><p align="left"> Daftar Masakan </p></h4>
        <a href="index"><button type="button" class="btn btn-primary round">Teratas</button></a>
        <?php 
        $select=mysqli_query($conn, "SELECT * FROM kategori");
        while($show=mysqli_fetch_array($select)){
        ?>
        <a href="view_order?page1&&id_kategori=<?php echo $show['id_kategori'];?>"><button type="button" class="btn btn-primary round <?php echo $show['id_kategori'];?>"><?php echo $show['nama_kategori'];?></button></a>
        <?php } ?>
             <div class="row"><br>
           <?php
                                    // Include / load file koneksi.php
                                    // Cek apakah terdapat data pada page URL
                                    $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                                    $limit = 4; // Jumlah data per halamanya

                                    // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
                                    $limit_start = ($page - 1) * $limit;

                                    // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                                    $data=mysqli_query($conn, "SELECT * FROM masakan where status_masakan='Y'LIMIT ".$limit_start.",".$limit);
                                    $no = $limit_start + 1; // Untuk penomoran tabel
                                    while($show=mysqli_fetch_array($data)){
                                    ?>
              <div class="col-lg-3 col-xs-12">
                  <div class="white-box alert-box">
                    <p><?php echo $show['nama_masakan'];?></p>
                    <p><img src='../admin/gambar/<?php echo $show['image']; ?>' width='150'></p>
                    <p><?php echo 'Rp '.number_format($show['harga']);?></p>
                    <button class="btn btn-primary" id="sa-basic"><a href ="cart?act=add&amp;id_masakan=<?php echo $show['id_masakan']; ?> &amp;ref=pemesanan"><font color ="white">Pesan</font></a></button>
                </div>
              </div>
<?php } ?>
               </div>
               <div class="pagination">
               <?php
            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
            ?>
                <li class="disabled"><a href="#">First</a></li>
                <li class="disabled"><a href="#">&laquo;</a></li>
            <?php
            } else { // Jika buka page ke 1
                $link_prev = ($page > 1) ? $page - 1 : 1;
            ?>
                <li><a href="pemesanan?page=1">First</a></li>
                <li><a href="pemesanan?page=<?php echo $link_prev; ?>">&laquo;</a></li>
            <?php
            }
            ?>

            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $sql2 = mysqli_query($conn,"SELECT COUNT(*) AS jumlah FROM masakan ");
            ($get_jumlah = (mysqli_fetch_array($sql2)));

            $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <li <?php echo $link_active; ?>><a href="pemesanan?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
            }
            ?>

            <!-- LINK NEXT AND LAST -->
            <?php
            // Jika page sama dengan jumlah page, maka disable link NEXT nya
            // Artinya page tersebut adalah page terakhir
            if ($page == $jumlah_page) { // Jika page terakhir
            ?>
                <li class="disabled"><a href="#">&raquo;</a></li>
                <li class="disabled"><a href="#">Last</a></li>
            <?php
            } else { // Jika bukan page terakhir
                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
            ?>
                <li><a href="pemesanan?page=<?php echo $link_next; ?>">&raquo;</a></li>
                <li><a href="pemesanan?page=<?php echo $jumlah_page; ?>">Last</a></li>
            <?php
            }
            ?>
            </div>
<form action="proses_masakan" method="post">
               <div class="container">  
               <div class="white-box"> 
               <div class="table-responsive">
<div class="title"><h3>Tabel Masakan Anda</h3></div>
            <div class="hero-unit">
            <tr>
            <?php
            if (isset($_SESSION['items1'])){
                foreach ($_SESSION['items1'] as $key => $val) {
                    $query = mysqli_query($conn, "SELECT * FROM meja where id_meja = '$key'");
                    $data = mysqli_fetch_array($query);
                    ?>
                    <input type="hidden" name="no_meja" value="<?php echo $data['id_meja'];?>">
                    <?php
                }
            }?>
                    <table class="table table-hover table-condensed">
                    <th><center>No</center></th>
                    <th><center>Nama Masakan</center></th>
                    <th><center>Harga</center></th>
                    <th><center>Quantity</center></th>
                    <th><center>Sub Total</center></th>
                    <th><center>Keterangan</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
    <?php
        //MENAMPILKAN DETAIL KERANJANG BELANJA//
    $no = 1;
    $total = 0;
    //mysql_select_db($database_conn, $conn);
    if (isset($_SESSION['items'])) {
        foreach ($_SESSION['items'] as $key => $val) {
            $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
            $data = mysqli_fetch_array($query);
      $jumlah_barang = mysqli_num_rows($query);
            $jumlah_harga = $data['harga'] * $val;
            $total += $jumlah_harga;
            $harga = $data['harga'];
            $hasil = "Rp.".number_format($harga,2,',','.');
            $hasil1 = "Rp.".number_format($jumlah_harga,2,',','.');
            ?>
                <tr>
                <td><center><?php echo $no++; ?></center></td>
                <td><center><input type="hidden" name="id_masakan[]" value="<?php echo $data['id_masakan']; ?>"><?php echo $data['nama_masakan'];?></center></td>
                <td><center><?php echo $hasil;?></center></td>
                <td><center><a href="cart?act=plus&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=entri_order" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></a>
                <input type="hidden" name="jumlah[]" value="<?php echo ($val);?>"><?php echo ($val);?>Pcs<a href="cart?act=min&amp;id_masakan=<?php echo $data['id_masakan'];?> &amp;ref=entri_order" class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></a>
                </center></td>
                <td><center><?php echo $hasil1;?></center></td>
                <td><center><textarea class="form-control" name="keterangan[]"></textarea></center></td>
                <td><center>
                <a href="cart?act=del&amp;id_masakan=<?php echo $data ['id_masakan'];?> &amp;ref=entri_order">Hapus</a>
                </center></td>
                </tr>
                

          <?php
                    //mysql_free_result($query);      
            }
              //$total += $sub;
            }?>
                        <?php
        if($total == 0){ ?>
          <td colspan="4" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
        <?php } else { ?>
                        <td colspan="8" style="font-size: 18px;"><b><div class="pull-right"><input type="hidden" value="<?php echo $total;?>" name="total_bayar"> Total Harga Anda : Rp. <?php echo number_format($total); ?>,- </div> </b></td>
          
        <?php 
        
        }
        ?>
                </table> 
                <p><div align="right">
            <button type="submit" class="btn btn-success">&raquo; Konfirmasi Pemesanan &laquo;</a></button>
                </form>
            </div>
            </div></p>
            </div>
     </div>
        <!-- End Wrapper-->
        </div>
        </div>
        <!--Start  Footer -->
        <footer class="footer-main"> 2017 &copy; Meter admin Template.  </footer> 
         <!--End footer -->

       </div>
      <!--End main content -->
    


    <!--Begin core plugin -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/plugins/moment/moment.js"></script>
    <script  src="assets/js/jquery.slimscroll.js "></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/functions.js"></script>
    <!-- End core plugin -->
    
    <!--Begin Page Level Plugin-->
  <script src="assets/plugins/morris-chart/morris.js"></script>
    <script src="assets/plugins/morris-chart/raphael-min.js"></script>
    <script src="assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="assets/pages/dashboard.js"></script>
    <!--End Page Level Plugin-->
   

</body>

</html>
