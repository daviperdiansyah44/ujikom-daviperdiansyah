<?php
include '../koneksi.php';
require('../assets/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('../assets/logo.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'D`Resto',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 088976082283',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jl. Paledang Kp. Karamat RT 05 RW 01 No. 26',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.dresto.com : dresto@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Pengguna",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Username', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Nama User', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Email', 1, 0, 'C');
$pdf->Cell(4.5, 0.8, 'Status', 1, 0, 'C');
$pdf->Cell(4.5, 0.8, 'Nama Jabatan', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($conn, "SELECT user.username, user.nama_user, user.email, level.nama_level, if((user.status)='Y','Aktif','Tidak Aktif') as status_user FROM user INNER JOIN level ON user.id_level = level.id_level where status = 'Y' and nama_level = 'pelanggan'");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['username'],1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['nama_user'], 1, 0,'C');
	$pdf->Cell(6, 0.8, $lihat['email'],1, 0, 'C');
	$pdf->Cell(4.5, 0.8, $lihat['status_user'], 1, 0,'C');
	$pdf->Cell(4.5, 0.8, $lihat['nama_level'],1, 1, 'C');

	$no++;
}

$pdf->Output("laporan_data_pengguna.pdf","I");

?>

