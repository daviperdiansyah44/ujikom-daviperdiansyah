<?php

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../admin/index");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index");
}
?>
<?php
include'../admin/database.php';
$db = new database();
?>
<?php
include('include/head.php');
?>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
       <?php include("include/body.php");?>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <?php include('include/menu.php');?>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            <form class="searchform">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                            <?php 
                            include "../koneksi.php";
                            $username=$_SESSION['username'];
                            $query_mysqli = mysqli_query($conn, "SELECT * FROM user where username='$_SESSION[username]'")or die(mysqli_error());
                            while($data = mysqli_fetch_array($query_mysqli)){
                          ?>
                          <?php echo $data['nama_user']; ?>
                          <?php } ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                          <li> <a href="logout"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">
        <div class="page-title-box">
                <h4 class="page-title">Data Order </h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                </ol>
                <div class="clearfix"></div>
             </div>
             <div class="row">
               <div class="col-md-8">
                    <div class="white-box">
           <?php
          include '../koneksi.php';
           $data_ambil_id_transaksi = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM transaksi where id_order='$_GET[id_order]'"));
 
          ?>
          <div class="box-body">
             <div class="agile3-grids">
             <?php
                $a = mysqli_query($conn,"SELECT keterangan_transaksi FROM transaksi WHERE id_order='$_GET[id_order]'");
                $b = mysqli_fetch_array($a);
                if($b[0]=="N"){
             ?>
             <a href="tambah_order?id_order=<?php echo $_GET['id_order'];?>"><button class="btn btn-success" type="submit"><i class="fa fa-shopping-cart">&nbsp;Tambah Pesanan</i></button></a>
            </button>
            <?php } ?>
          </div>
          </div>
        </br>
<div class="table-responsive">

       <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Masakan</th>
                  <th>Harga</th>
                    <th>Quantity</th>
                   <th>Keterangan</th>
                   <th>Jumlah</th>
                   
                 

                </tr>
              </thead>
              <?php
              error_reporting(0);
              $no = 1;
              $total=0;
             
              foreach($db->detail_tampil() as $x){
               $harga=$x['harga'];
                $jumlah=$x['jumlah']*$x['harga'];
                  $hasil="Rp.".number_format($harga,2,',','.');
                   $jml=$x['jml'] *$harga;
                    $jumlah1="Rp.".number_format($jml,2,',','.');
                ?>
                <tbody>

                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $x['nama_masakan']; ?></td>
                    <td><?php echo $hasil; ?></td>
                    <td><?php echo $x['jml']; ?></td>
                    <td><?php echo $x['keterangan']; ?></td>
                    <td><?php echo $jumlah1;?></td>

                    

                  </tr>
                 

                </tbody>
                <?php 
                $total += ($jml) ;
                 $total1="Rp.".number_format($total,2,',','.');
              }
              ?>
                                         <tr>


  <td colspan="5" align="right">Total</td>
  <td ><?php echo $total1;?></td>
 
  </tr>
            </table>
          </div><!-- /.box-body --><br>
        </div><!-- /.box -->
      </div><!-- /.col -->
      <div class="col-md-4">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="white-box">
             <?php
             include "../login/koneksi.php";
$query_edit = mysqli_query($conn,"SELECT *, oder.tanggal FROM oder inner join user on oder.id_user=user.id_user inner join transaksi on oder.id_order=transaksi.id_order where oder.id_order='$_GET[id_order]'");
$x = mysqli_fetch_array($query_edit)
?>
                  <h3 class="box-title">Data Order NO Meja <?php echo $x['no_meja']; ?></h3><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">No Meja</label>
                      <input class="form-control" value="<?php echo $x['no_meja']; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Pembeli</label>
                      <input  class="form-control" value="<?php echo $x['nama_user']; ?>" readonly>
                    </div>
                         <div class="form-group">
                      <label for="exampleInputEmail1">Tanggal</label>
                      <input class="form-control" value="<?php echo $x['tanggal']; ?>" readonly>
                    </div>
                      <div class="form-group">
                      <label for="exampleInputEmail1">Keterangan Pembayaran</label>
                    <?php
                                            if($x['keterangan_transaksi'] == 'Y')
                                            {
                                              ?>
                                         
                      <input class="form-control" value="<?php echo "Terbayar"; ?>" readonly>
                    
                                          
                                            <?php
                                            }else{
                                              ?>
                                           
                     
                      <input class="form-control" value="<?php echo "Belum Terbayar"; ?>" readonly>
                    </div>
                                            <?php 
            }
            ?>
                     
                  </div><!-- /.box-body -->
                </form>
                </div>
              </div><!-- /.box -->
            </div>


               </tr>
                                        </tr>
                                    </tbody>
                                   </table>
                            </div></div>


        <!--Start  Footer -->
<?php 
include('include/footer.php');
?>
    <!--End Page Level Plugin-->
   

</body>

</html>
