<?php
    session_start();
    include "../koneksi.php";

include ('function.php');
	
if(isset($_POST['submit']))
{
	$email = $_POST['email'];
	$email = mysqli_real_escape_string($conn, $email);
	
	if(checkUser($email) == "true")
	{
		$userID = UserID($email);
		$token = generateRandomString();
		
		$query = mysqli_query($conn, "INSERT INTO recovery_keys (userID, token) VALUES ($userID, '$token') ");
		if($query)
		{
			 $send_mail = send_mail($email, $token);


			if($send_mail === 'success')
			{
				 $msg = 'A mail with recovery instruction has sent to your email.';
				 $msgclass = 'bg-success';
			}else{
				$msg = 'There is something wrong.';
				$msgclass = 'bg-danger';
			}



		}else
		{
				$msg = 'There is something wrong.';
				 $msgclass = 'bg-danger';
		}
		
	}else
	{
		$msg = "This email doesn't exist in our database.";
				 $msgclass = 'bg-danger';
	}
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <title>D'Resto</title>
        <meta name="description" content="Custom Login Form Styling with CSS3" />
        <meta name="keywords" content="css3, login, form, custom, input, submit, button, html5, placeholder" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../admin/assets/pdf/logo.png"> 
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script src="js/modernizr.custom.63321.js"></script>
        <!--[if lte IE 7]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
		<style>
			body {
				background: #e1c192 url(images/wood_pattern.jpg);
			}
		</style>
    </head>
    <body>
        <div class="container">
			
			<header><br><br><br><br><br><br>
			
				<h1><strong>Lupa Password?</strong></h1>
				
			</header>
			
			<section class="main">
				<form action ="" method="post" class="form-2">
<?php if(isset($msg)) { ?>
    <div class="<?php echo $msgclass; ?>" style="padding:5px;"><?php echo $msg; ?></div>
<?php } ?>
					<p class="float">
						<label for="username"><i class="icon-envelope"></i>Masukan Email</label>
						<input style="width: 200%" type="email" name="email" placeholder="Email"><br><p style="padding-top: 80px;" align="right"><a href="index">Kembali</a>
					</p>
					<p class="clearfix">    
						<input type="submit" name="submit" value="Kirim Password">
					</p>
				</form>​​
			</section>
			
        </div>
		<!-- jQuery if needed -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    </body>
</html>